from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from .models import Todo


def todos(request):
    todo_lists = Todo.objects.all()
    template_name = 'todo.html'
    if request.method == 'POST':
        Todo.objects.create(todo=request.POST.get('todos'))
    return render(request, template_name, {'todos': todo_lists})


def del_todo(request, id):
    selected_todo = Todo.objects.get(id=id)
    selected_todo.delete()
    return redirect('todos')


def complete_todo(request, id):
    selected_todo = Todo.objects.get(id=id)
    if not selected_todo.done:
        selected_todo.done = True
    else:
        selected_todo.done = False
    selected_todo.save()
    return redirect('todos')
