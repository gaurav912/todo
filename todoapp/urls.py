from django.urls import path
from . import views
urlpatterns = [
    path('', views.todos, name='todos'),
    path('todos/<int:id>/completed/', views.complete_todo, name='todo_complete'),
    path('todos/<int:id>/delete/', views.del_todo, name='todo_delete'),
]
